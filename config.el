;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!
(add-to-list 'initial-frame-alist '(fullscreen . fullboth))

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Muhannad Alrusayni"
      user-mail-address "Muhannad.Alrusayni@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "DejaVu Sans Mono" :size 24 :weight 'regular)
      doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; Set second input method to Arabic
(setq-default default-input-method "arabic")

;; Set default dictionary
(setq-default ispell-dictionary "en_US")

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; set my default project path
;; (setq projectile-project-search-path (f-entries "/home/muhannad/workspace"))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Re-bind local leader kay
(setq doom-localleader-key ",")

(use-package! lsp
  :config
  (setq! lsp-ui-doc-enable nil
         lsp-signature-auto-activate nil))

;; (use-package! flutter
;;   :defer t
;;   :init
;;   (map! :map dart-mode-map
;;         :localleader
;;         :desc "Run" "r" #'flutter-run-or-hot-reload))

;; (use-package! lsp-dart
;;   :config
;;   (setq ; lsp-dart-sdk-dir "/var/lib/snapd/snap/bin/"
;;         lsp-dart-flutter-sdk-dir "/home/muhannad/snap/flutter/common/flutter"))

(use-package! lsp-rust
  :config
  (map! :map rustic-mode-map
        :localleader
        :desc "Goto online docs" "h" #'lsp-rust-analyzer-open-external-docs)
  (setq! +format-on-save-enabled-modes '(rustic-mode))
  (setq! lsp-rust-analyzer-proc-macro-enable t
         lsp-rust-analyzer-cargo-load-out-dirs-from-check t
         lsp-rust-clippy-preference "off"))

(after! org
  (map! :map org-mode-map
      :localleader
      :desc "Babel" "c" org-babel-map))

(map! :g
      ;; drag stuff remap
      "M-j" 'drag-stuff-down
      "M-k" 'drag-stuff-up
      "M-l" 'drag-stuff-right
      "M-h" 'drag-stuff-left
      ;; hide every thing in this level
      :n "zC" 'hs-hide-level
      :leader
      ;; generate uuid
      :n "iU" 'uuidgen
      ;; generate lorem paragraph
      :n "il" 'lorem-ipsum-insert-paragraphs)

(after! web-mode
  (map! :map web-mode-map
        ;; bring back my autocompelte :/
        :g "M-/" nil))


(use-package! vertico
  :config
  (setq vertico-resize nil
        vertico-count 8))

(setq company-async-redisplay-delay 1
      company-box-doc-delay 1.7)

(require 'quickrun)

(setq pyvenv-default-virtual-env-name "venv")

(require 'lsp-java-boot)

;; to enable the lenses
(add-hook 'lsp-mode-hook #'lsp-lens-mode)
(add-hook 'java-mode-hook #'lsp-java-boot-lens-mode)
(add-hook 'sql-mode-hook (lambda () (setq! tab-width 4)))

(setq lsp-java-import-maven-enabled t)


(setq markdown-fontify-code-blocks-natively t)

(use-package! kubernetes
  :config
  (map! :leader
        :desc "Kubernetes" "o k" #'kubernetes-overview))

(use-package! kubernetes-evil
  :after-call kubernetes-overview)
